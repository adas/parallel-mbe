/*
 * ParLmbc.h
 *
 *  Created on: Jan 5, 2019
 *      Author: apurba
 */

#ifndef SOURCE_PARLMBC_H_
#define SOURCE_PARLMBC_H_

#include "global.h"

class ParLmbc {
	undirectedgraph _g_;
	int ms;
	long offset = 10e9;
	tbb::atomic<long> maximal_biclique_count;
	string of;
	void ParMineLMBC(tbb::concurrent_unordered_set<int>&, tbb::concurrent_unordered_set<int>&, tbb::concurrent_unordered_set<int>&, int);
	void ParMineLMBC_modified(tbb::concurrent_unordered_set<int>&, tbb::concurrent_unordered_set<int>&, tbb::concurrent_unordered_set<int>&, int);
	void ParMineLMBC(tbb::concurrent_unordered_set<int>&, tbb::concurrent_unordered_set<int>&, tbb::concurrent_unordered_set<int>&, int, int);
	void MineLMBC(tbb::concurrent_unordered_set<int>&, tbb::concurrent_unordered_set<int>&, tbb::concurrent_unordered_set<int>&, int);
	void MineLMBC(tbb::concurrent_unordered_set<int>&, tbb::concurrent_unordered_set<int>&, tbb::concurrent_unordered_set<int>&, int, int);
	void MineLMBCSeq(unordered_set<int>&, unordered_set<int>&, unordered_set<int>&, int, int);
	void CDFS(undirectedgraph &, unordered_set<int>&, tbb::concurrent_unordered_set<int>&, tbb::concurrent_unordered_set<int>&, int, int);
	void compute_half_intersection(tbb::concurrent_unordered_set<int>&, unordered_set<int> *);
public:
	ParLmbc();
	ParLmbc(undirectedgraph &);
	ParLmbc(undirectedgraph &, int);
	void run(string);
	void runParMBE(string, int);
	void runParMBESeq(string);
	void runCDFS(string);
	long getCount();
	virtual ~ParLmbc();
};

#endif /* SOURCE_PARLMBC_H_ */
