#include <iostream>

using namespace std;

#include "source/Lmbc.h"
#include "source/ParLmbc.h"
#include "source/undirectedgraph.h"

int main(int argc, char** argv){

	undirectedgraph g;

	g.readInBiEdgeList(argv[1]);

	cout << g.numV() << "\n";

	cout << g.numE() << "\n";



	cout << "Sequential MBE algorithm\n\n\n";

	Lmbc lmbc(g);

	lmbc.run();

	cout << "number of maximal biclique found: " << lmbc.getCount() << "\n\n";

	cout << "Parallel MBE algorithm\n\n\n";

	ParLmbc parlmbc(g);
	parlmbc.run();

	cout << "number of maximal biclique found: " << parlmbc.getCount() << "\n\n";


}
